﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Account
    {
        [Required]
        [DataType(DataType.Password)]
        
        public String Password { set; get; }

        [Required]
        public String Email { set; get; }
        [Required]

        [MaxLength(100, ErrorMessage = "Co toi da 100 ki tu")]
        public String FirstName { set; get; }

        [MaxLength(100, ErrorMessage = "Co toi da 100 ki tu")]
        public String LastName { set; get; }
       
        [DataType(DataType.EmailAddress)]
        public int AccountId { set; get; }
        public virtual ICollection<Post> Post { set; get; }
    }
}