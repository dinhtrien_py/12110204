﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Tag
    {
        public int Id { set; get; }
        
        [StringLength(100, ErrorMessage = "So luong ki tu tu 10-100 ki tu", MinimumLength = 10)]
        public String Content { set; get; }
        [Required]

        public virtual ICollection<Post> Posts { set; get; }

    }
}