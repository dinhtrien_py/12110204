﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Comment
    {
        public int Id { set; get; }

        [Required]
        public String Body { set; get; }

        [DataType(DataType.Date,ErrorMessage ="Nhap Dung ngay nhe")]
        public DateTime DateCreated { get; set; }

        [DataType(DataType.Date, ErrorMessage = "Nhap Dung ngay nhe")]
        public DateTime DateUpdate { get; set; }
        //public String PostId { set; get; }
        public String Author { get; set; }

        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}