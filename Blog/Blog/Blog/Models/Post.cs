﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    //bo "s" sau Posts
    [Table("Post")]
    public class Post
    {
        public int Id { set; get; }
        //[Required]
        
        [StringLength(500, ErrorMessage = "So luong ki tu tu 20-500 ki tu", MinimumLength = 20)]
        public String Title { set; get; }
        //[Required]
        
        [MinLength (50,ErrorMessage ="it nhat 50 ki tu")]
        public String Body { set; get; }
        //[Required]

        
        [DataType(DataType.Date, ErrorMessage = "Chua chon ngay")]
        public DateTime DateCreated { get; set; }
        //[Required]
        
        [DataType(DataType.Date, ErrorMessage = "Chua chon ngay")]
        public DateTime DateUpdate { get; set; }
        
        public virtual ICollection<Comment> Comments { set; get; }

        public String AccountID { set; get; }
        public virtual Account Account { set; get; }

        public virtual ICollection<Tag> Tags { set; get; }

    }
}