namespace Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan53 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Post", "Body", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Post", "Body", c => c.String());
        }
    }
}
