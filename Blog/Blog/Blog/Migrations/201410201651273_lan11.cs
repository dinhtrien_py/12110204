namespace Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan11 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Comments", "Body", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Comments", "Body", c => c.String());
        }
    }
}
