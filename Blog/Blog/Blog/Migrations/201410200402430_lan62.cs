namespace Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan62 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Post", "Title", c => c.String(maxLength: 500));
            AlterColumn("dbo.Post", "Body", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Post", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.Post", "Title", c => c.String(nullable: false, maxLength: 500));
        }
    }
}
