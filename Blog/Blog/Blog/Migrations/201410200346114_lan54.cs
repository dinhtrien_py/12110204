namespace Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan54 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Post", "Body", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Post", "Body", c => c.String(nullable: false));
        }
    }
}
