namespace Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan61 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Post", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.Post", "AccountID", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Post", "AccountID", c => c.String(nullable: false));
            AlterColumn("dbo.Post", "Body", c => c.String());
        }
    }
}
