namespace Blog_InternetApplications.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan12 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Comments", "DateUpdate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Comments", "DateUpdate", c => c.DateTime(nullable: false));
        }
    }
}
