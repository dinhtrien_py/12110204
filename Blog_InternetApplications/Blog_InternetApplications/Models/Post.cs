﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_InternetApplications.Models
{
    public class Post
    {
        [Required]
        public int ID { set; get; }

        [Required]
        [StringLength(500, ErrorMessage = "So luong ki tu tu 20-500 ki tu", MinimumLength = 20)]
        public String Title { set; get; }

        [Required]
        [MinLength(50, ErrorMessage = "it nhat 50 ki tu")]
        public String Body { set; get; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }

        //1-n account
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId{ set; get; } //Khoa ngoai

        //1-n comment
        public virtual ICollection<Comment> Comments { set; get; }

        //1-n Tag
        public virtual ICollection<Tag> Tags { set; get; }
    }
}